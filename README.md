`"pls.opt"` is an [R](http://www.r-project.org/) package dedicated to Partial Least Squares Path Modeling (PLS-PM) analysis for *metric* data.

## Requirements
```ruby
options("install.lock" = FALSE) # avoid lock error

# install "devtools"
install.packages("devtools", dependencies = TRUE)
library(devtools)

# install "plspm"
install_github("gastonstat/plspm")
install.packages(pkgs = c("yaml", "broom", "xtable",
                          "tree", "rpart", "rpart.plot",
                          "igraph", "dagitty"),
                 dependencies = TRUE)
```

## Life Cycle
This package implements a Tree PLS procedure to pick a preferred PLS path model. This package specifies the measurement models as a single-item and thus considers only the path model, as induced by the structural model of PLS-PM. This model selection procedure is summarized as follows :

1.  Randomly partition the original dataset into two samples - training and test samples.
2.  Estimate a decision tree model using the training sample, and build the representative digraph. The function _graph\_gen_ saves the resulting decision tree (_**dt\_graph.png**_), and the adjacency matrix describes the associations among variables (_**dt\_graph\_matrix.csv**_).
3.  To generate an acyclic digraph (DAG), run Depth-First     search from any vertex (i.e., starting node) on the       representative digraph and filter out only backward       edges. The function _graph\_opt_ saves the following      result files:

    *  All explored edges are classified into four types (_**edges\_list.csv**_)
    *  The discovery and finish times for each vertex (_**search\_order.csv**_)

4.  For each induced DAG, estimate the PLS path model         using the same training sample. The function              _pls\_estimate_ estimates two PLS path models - the       full model, induced DAG, and reduced model, without       the non-significant edges. The wrapper function for       _plspm_ saves the following result files :

    *  The recursive model, including the coefficients of        edges (suffix _**\_dag.png**_)
    *  Path matrix with coefficients (suffix                     _**\_matrix.csv**_)
    *  Path effects (either suffixes _**\_list.csv**_ or _**\_sum.tex**_)

5.  Apply the point prediction (_pls\_predict_) using the holdout sample, where the path model is predicted sequentially by topological order (i.e., follows the path model forward, as described by Galit Shmueli, 2016).

The function _point\_pred\_pm_ summarizes the results for each recursive model, which is noted by the proper starting node in the DFS algorithm :

*  Summarized measures of PLS path model - Goodness-of-Fit, as obtained by training sample, and RMSPE for the response variable, as obtained by the test sample (_**dags\_metrics.tex**_).
*  All the backward edges drop from the representative digraph at each DAG (_**dags\_diff.tex**_).

Steps 3-5 result in a separate directory by the proper noted model, the starting node.


The cross-validation procedure is applied for each recursive model, which is noted by the proper starting node in the DFS algorithm, so the steps 4-5 are run for each fold. The results summarize the measures of PLS path model - Goodness-of-Fit, as obtained by training sample, and in-sample and out-of-sample RMSPE for the response variable, as obtained by the test sample (_**dags\_K\_folds\_validation.csv**_).

## Running
The configuration file _**conf.yaml**_ defines the following main arguments :

1.  Path to dataset file (data\_file)
2.  Path to variables' specifications  (vars\_spec\_file)
3.  Desired response variable (target\_var)
4.  Path to a directory for results (results\_dir)

The file _flow\_pp.R_ applies the above procedure using the configuration file _**sim\_conf.yaml**_, which contains the above arguments together with the following arguments :

5.  Specified seed (run\_seed)
6.  Specified ratio for splitting dataset (ratio\_split)

The file _flow\_cv.R_ applies a cross-validation procedure for each recursive model, as constructed by depth-first search. This procedure uses the same configuration file _**sim\_conf.yaml**_, added to the number of folds (**K**) argument.

## Authors Contact
[Ziv Litmanovitz](https://www.linkedin.com/in/litmanovitziv/) (`litmanovitziv at gmail.com`)
