
source("R/preprocess_data.R")
source("R/tree_pls.R")

write.tex <- function(sum_obj, sum_name, row_names = TRUE) {
  digits_conf <- mapply(function(c_name) {if (is.numeric(sum_obj[,c_name])) 7 else 0}, names(sum_obj))
  print(xtable(sum_obj, digits = c(0, unname(digits_conf))),
        file = file.path(getwd(), sum_name),
        include.rownames = row_names)
}

sim_conf <- unlist(read_yaml("sim_conf.yaml"))
input <- read_data(file.path(sim_conf["data_file"]))
head(input$data)
str(input$data)
target_var <- sim_conf["target_var"]

run_seed <- as.numeric(sim_conf["run_seed"])
set.seed(run_seed)
ratio_split <- as.numeric(sim_conf["ratio_split"])
sim_dir <- file.path(run_seed, ratio_split*100)
run_dir <- file.path(sim_conf["results_dir"], sim_dir)
K_folds <- as.numeric(sim_conf["K"])

# K-Fold Cross Validation for each model
setwd(run_dir)
sources <- list.dirs(path = run_dir, full.names = FALSE, recursive = FALSE)
pred_acc <- cross_valid_pm(K = K_folds, input_data = input$data,
                           target_var = target_var, models_names = sources, run_dir)
# dags_sum_models <- dags_sum_models[order(dags_sum_models$s_node, decreasing = FALSE),]
write.tex(pred_acc, paste("dags", K_folds, "folds_validation.tex", sep = "_"), row_names = FALSE)
write.csv(pred_acc, paste("dags", K_folds, "folds_validation.csv", sep = "_"), row.names = FALSE)


