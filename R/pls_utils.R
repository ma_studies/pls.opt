library(broom)
library(xtable)

#' @details create paths' table for inner and outer model
#' @param path_matrix
#' @param target_vars
#' @param sep_struct indicator to separate variables between tables
#' @return 
create_path_model <- function(path_matrix, target_vars, sep_struct = FALSE) {
  if (sep_struct) {
    LV_names <- paste(colnames(path_matrix), "LV", sep = "_")
  } else {
    LV_names <- colnames(path_matrix)
  }
  
  smMatrix <- path_matrix
  colnames(smMatrix) <- rownames(smMatrix) <- LV_names
  
  exogenous_vars <- colnames(path_matrix)[!colnames(path_matrix) %in% target_vars]
  exo_mmMatrix <- data.frame(measurement = exogenous_vars,
                             latent = if (!sep_struct) exogenous_vars else paste(exogenous_vars, "LV", sep = "_"),
                             type = rep("F", length(exogenous_vars)),
                             stringsAsFactors = FALSE)
  endo_mmMatrix <- data.frame(measurement = target_vars,
                              latent = if (!sep_struct) target_vars else paste(target_vars, "LV", sep = "_"),
                              type = rep("R", length(target_vars)),
                              stringsAsFactors = FALSE)
  mmMatrix <- rbind(exo_mmMatrix, endo_mmMatrix)
  mmMatrix[mmMatrix$type == "F", "symbol"] <- "B"
  mmMatrix[mmMatrix$type == "R", "symbol"] <- "A"
  
  smMatrix <- as.data.frame(as.table(smMatrix), stringsAsFactors = FALSE)
  colnames(smMatrix) <- c("source", "target", "weight")
  smMatrix <- smMatrix[smMatrix$weight != 0,]
  
  return(list("measurement" = mmMatrix, "structure" = smMatrix))
}

#' @details concatenate all tables of all path coefficients
#' @param path_coef list of summary tables
#' @return single data frame includes all path coefficients
flatten_path_coef <- function(path_coef) {
  direct_path <- data.frame(matrix(NA, nrow = 0, ncol = 6))
  for (target in names(path_coef)) {
    curr_result <- path_coef[[target]]
    curr_result <- cbind(target, row.names(curr_result),
                         data.frame(curr_result, row.names = NULL),
                         stringsAsFactors = FALSE)
    direct_path <- rbind(direct_path, curr_result[-1,])
  }
  names(direct_path) <- c("target", "source", "path_coef", "sd", "t.value", "p.value")
  return(direct_path)
}

#' @details extract path effect table
#' @param path_effect summary table of direct and indirect effect
extract_path_eff <- function(path_effect) {
  path_tbl <- apply(path_effect, 1, function(r){
    return(strsplit(r[1], split = " -> ", fixed = TRUE)[[1]])
  })
  
  sum_effect <- cbind(t(path_tbl), path_effect, stringsAsFactors = FALSE)
  names(sum_effect)[1:2] <- c("source", "target")
  return(sum_effect)
}

#' @details 
#' @param path_tbl
#' @param prefix_file_name
#' @param results_dir full path of directory to write path matrix and plotted graph
save_inner_model <- function(path_tbl, prefix_file_name, results_dir = getwd()) {
  path_name <- paste(results_dir, prefix_file_name, sep = "/")
  if (is.matrix(path_tbl)) {
    path_tbl <- as.data.frame(as.table(path_tbl), stringsAsFactors = FALSE)
    colnames(path_tbl) <- c("source", "target", "weight")
    path_tbl <- path_tbl[path_tbl$weight != 0,]
  }
  
  digits_conf <- mapply(function(c_name) {if (is.numeric(path_tbl[,c_name])) 5 else 0}, names(path_tbl))
  print(xtable(path_tbl, digits = c(0, unname(digits_conf)),
               caption = paste(prefix_file_name, "sum", sep = "_")),
        file = if (!is.null(results_dir)) paste0(paste(path_name, "sum", sep = "_"), ".tex"),
        include.rownames = FALSE)
  write.csv(path_tbl, file = paste0(paste(path_name, "list", sep = "_"), ".csv"), row.names = FALSE, na = "")
}

#' @details
#' @param loadings_matrix
#' @param weights_matrix
#' @param prefix_file_name
#' @param results_dir full path of directory to write path matrix and plotted graph
save_model_mats <- function(mats_list, prefix_file_name, results_dir = getwd()) {
  path_name <- paste(results_dir, prefix_file_name, sep = "/")
  for (data_name in names(mats_list)) {
    write.csv(mats_list[[data_name]], file = paste0(paste(path_name, data_name, sep = "_"), ".csv"), na = "")
  }
}

#' @details estimate linear model and extract its indicies
#' @param input_data 
#' @param response_var 
#' @param predictors_vars
#' @param index the dedicated examined index by lm function
#' @return 
model_estimation <- function(input_data, response_var, predictors_vars, index = NULL) {
  covariates <- names(predictors_vars[predictors_vars])
  input_data <- input_data[,c(covariates, response_var), drop = FALSE]
  # path_formula <- paste(covariates, collapse = " + ")
  path_formula <- as.formula(paste(response_var, ".", sep = " ~ "))
  model.fit <- lm(formula = path_formula, data = input_data)
  path_coef <- as.data.frame(tidy(model.fit))
  rownames(path_coef) <- path_coef$term
  path_coef <- path_coef[,-1]

  if (!is.null(index)) {
    coef_index <- setNames(path_coef[-1,index], row.names(path_coef)[-1])
    coef_index[response_var] <- 0
    predictors_vars <- predictors_vars[!predictors_vars]
    predictors_vars[] <- 0
    coef_index <- c(coef_index, predictors_vars)
    coef_index <- coef_index[order(as.numeric(substring(names(coef_index), 2)))]
  } else {coef_index <- NULL}
  
  return(list(path_coef = path_coef,
              path_vec = coef_index,
              path_r2 = summary(model.fit)$r.squared))
}
